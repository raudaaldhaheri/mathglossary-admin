import arTranslations from './arTranslations';
import enTranslations from './enTranslations';


export default {
  ar: arTranslations,
  en: enTranslations,
};
