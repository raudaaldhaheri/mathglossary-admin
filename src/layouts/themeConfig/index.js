import createTheme from 'styled-components-theme';

export const COLORS = {
  primary: 'green',
  secondry: '#5A6169',
  success: 'green',
  info: '#00B8D8',
  warn: '#FFB400',
  danger: '#d60000',
  paragraphsColor: '#505050',
  headersColor: '#909090',
  light: '#E9ECEF',
  dark: '#212529',
  brand: '#e82510',
}

export const themeColors = createTheme(...Object.keys(COLORS));

export default {
  colors: {
    ...COLORS,
  },
  sizes: {
    radius: '0',
  }
}