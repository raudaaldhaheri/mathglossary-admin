import React from 'react'
import Api from 'Api';

import routers from 'routes/Public';
import { Switch, Route, Redirect } from 'react-router-dom';
import logo from 'assets/img/mohesr-logo.png';

export default () => {
  Api.defaults.headers['Authorization'] = undefined;
  return (
    <div style={{
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'space-around',
      alignItems: 'center',
      alignContent: 'center',
      flex: 1,
      height: '100vh'
    }}>
      <img width={150} src={logo} alt="diar" />
      <div style={{ textAlign: 'center' }}>
        <Switch>
          {routers.map((route, index) => {
            if (route.redirect) return <Redirect key={index} to={route.path} />;
            return <Route key={index} component={route.component} path={route.path} />
          })}
        </Switch>
      </div>
      <div className="diar-rights">
        &copy; BenchMark 2019 : {new Date().getFullYear()}
      </div>
    </div>
  )
}
