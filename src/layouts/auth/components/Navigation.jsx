import React from 'react';
import _ from 'lodash';
import { NavLink } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import routers from 'routes/Private';
import { A, NavConatiner } from './styled';

const Nav = ({ sidebarName, isAdmin, path, role, icon }) => {
  if ((isAdmin && role > 1) || !isAdmin) return (
  <NavLink activeClassName="active" className="link" to={path}>
    <FontAwesomeIcon className="icon" icon={icon} />
    <span className="text">{sidebarName}</span>
  </NavLink>
  );
  return false;
};

const StaticNav = ({ path, sidebarName, icon, handleClick }) => (
  <A onClick={() => handleClick(path)} role="button" className="link">
    <FontAwesomeIcon className="icon" icon={icon} />
    <span className="text">{sidebarName}</span>
  </A>
);

export default class Navigation extends React.Component {
  state = { hidden: {} }
  toggle = (path) => {
    this.setState(prevState => ({
      ...prevState,
      ...prevState.hidden,
      [path]: !this.state[path],
    }));
  }

  render() {
    const { role } = this.props;
    return (
      <NavConatiner>
        {routers.map((prop, key) => {
          if (prop.redirect) return null
          if (_.isArray(prop.subMenu)) {
            return <React.Fragment key={key}>
              <StaticNav handleClick={ this.toggle } {...prop} />
    
              {prop.subMenu.map((item, index) => (
                this.state[prop.path] && <Nav role={role} {...item} key={index}/>
              ))}
            </React.Fragment>
          }
          return (
            <Nav {...prop} role={role} key={key} />
          )
        })}
      </NavConatiner>
    )
  }
}
