import styled from 'styled-components';

export const A = styled.a`
  display: block
`;

export const NestedNav = styled.a`
  display: block;  
  background: black;
`;

export const NavConatiner = styled.div`
  @media screen and (max-width: 450px) {
    margin-top: 5rem;
  }
  margin-top: 1rem;
`;

export const Nav = styled.div`
  @media screen and (max-width: 450px) {
    margin-right: auto;
    height: 5rem;
    width: 100%;
    .logo {
      height: 5rem !important;
      display: flex;
      align-items: center;
      img {
        height: 2rem;
        margin: 0 .5rem;
      }
    }
    &:hover {
      height: 100vh;
    }
  }

  height: 100vh;
  overflow: hidden;
  position: fixed;
  width: 4rem;
  display: flex;
  z-index: 100;
  flex-direction: column;
  justify-content: space-between;
  box-shadow: 0 0.125rem 0.3125rem rgba(0,0,0,.26);
  background-color: white;
  color: #eee;
  transition: all 200ms;

  &:hover {
    width: 14rem;
      .logo {
        display: flex;
        align-items: center;
      }
  }

  .profile-info {
    .img-container {
      width: 2.5rem;
      height: 2.5rem;
      display: flex;
      border-radius: 50%;
      margin: 0 .75rem;
      background-color: #fff;
      border: 1px solid rgba(#eee, .3);
      box-sizing: border-box;
      img {
        width: 100%;
      }
    }
    &:hover,
    .pop-up:hover {
      .pop-up {
        display: block;
        opacity: 1;
      }      
    }
    .pop-up {
      display: none;
      opacity: 0;
      transition: all ease-in 1s 1s;      
      position: absolute;
      top: -4rem;
      cursor: pointer;
      padding-bottom: 2rem;
      &> div {
        border-radius: .5rem;
        border: 1px solid #eee;
        width: 7rem;
        box-shadow: $shadow;
        text-align: center;
      }
    }
  }

  .logo {
    height: 6rem;
    display: flex;
    align-items: center;
    img {
      height: 2rem;
      margin: 0 .5rem;
    }
  }

  .link {
    font-size: 1.2rem;
    align-items: center;
    display: flex;
    text-decoration: none;
    cursor: pointer;
    color: ${({ theme }) => theme.colors.headersColor};
    height: 2rem;
    svg {
      width: 4rem;
    }
    &:hover {
      background-color: #eee;
      color: ${({ theme }) => theme.colors.primary};
    }
    &.active {
      .text,
      svg {
        color: #000;
      }
    }
    img {
      width: 3rem;
    }
  }
`

export const Profile = styled.div`
  position: relative;
  margin-bottom: 5px;
`

export const Main = styled.div`
  display: flex;
  flex-direction: column;
  padding: 2rem;
  margin-right: 4rem;
  overflow: auto;
  @media screen and (max-width: 450px) {
    margin-right: auto;
  }
`