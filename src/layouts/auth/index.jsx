import React from 'react';
import { Switch, Redirect, Route, withRouter } from "react-router-dom";
import { connect } from 'react-redux';
import routers from 'routes/Private';
import _ from 'lodash';
import logo from 'assets/img/mohesr-logo.png';
import Header from 'components/Header';
import Navigation from './components/Navigation';
import UserInfo from './components/UserInfo';
import { Nav, Main } from './components/styled';

const AuthLayout = ({role, logout}) => (
  <div dir="rtl">
    <div className="App">
      <Nav>
        <div>
          <div className="logo">
            <img src={logo} alt="diar app" />
          </div>
          <Navigation role={role} />
        </div>
        <div>
          <UserInfo />
          <div role="button" style={{ background: '#eee', color: 'red', cursor: 'pointer' }} onClick={logout}>تسجيل الخروج</div>
        </div>
      </Nav>
      <Main>
        <Header />
        <div className="main-container">
          <Switch>
            {routers.map((router, index) => {
              if (router.redirect) {
                return <Redirect key={index} to={router.path} />
              }
              if (_.isArray(router.subMenu)) {
                return router.subMenu.map((item, i) => (
                  <Route key={`${index}-${i}`} component={item.component} path={item.path} />
                ))
              }
              return <Route key={index} component={router.component} path={router.path} />
            })}
          </Switch>
        </div>
      </Main>
    </div>
  </div>
);

const mapStateToProps = ({ auth: { role } }) => ({
  role,
})

const mapDispatchToProps = (dispatch) => ({
  logout: () => dispatch({ type: 'LOGOUT' })
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AuthLayout))