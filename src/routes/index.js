import React from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import Api from 'Api';

import Private from './Private';
import Public from './Public';
import { toast } from 'react-toastify';

const Routes = ({ token }) => {
  if (!token) toast.error(token)
  console.log({token})
  return token ? <Private /> : <Public />;
}

const mapStateToProps = ({ auth: { token }}) => ({ token })

export default withRouter(connect(mapStateToProps)(Routes));
