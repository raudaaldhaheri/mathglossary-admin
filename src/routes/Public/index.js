import Login from 'containers/unauth/Login';
import ConfirmEmail from 'containers/unauth/ConfirmEmail';

export default [
  {
    path: "/login",
    component: Login
  },
  {
    path: "/:email_confirmation_token",
    component: ConfirmEmail
  },
  { redirect: true, path: '/login' }
];
