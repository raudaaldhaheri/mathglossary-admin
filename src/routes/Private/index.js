import {
  faUser,
  faEnvelope,
  faAtom,
  faUniversalAccess,
  faLink,
  faAdjust,
} from '@fortawesome/free-solid-svg-icons';

import Users from 'containers/auth/Users';
import Teachers from 'containers/auth/Teachers';
import Terms from 'containers/auth/Terms';
import Applications from 'containers/auth/Applications';
import Testsites from 'containers/auth/Testsites';
import SuggestionLinks from 'containers/auth/SuggestionLinks';
import Feedbacks from 'containers/auth/Feedbacks';

export default [
  {
    path: '/mathwords',
    sidebarName: 'المصطلحات',
    icon: faUser,
    component: Terms,
  },
  {
    path: '/applications',
    sidebarName: 'التطبيق',
    icon: faAtom,
    component: Applications,
  },
  {
    path: '/testsites',
    sidebarName: 'موقع الاختبار',
    icon: faAdjust,
    component: Testsites,
  },
  {
    path: '/suggestion_links',
    sidebarName: 'الروابط المقترحة',
    icon: faLink,
    component: SuggestionLinks,
  },
  {
    path: '/users',
    sidebarName: 'المستخدمين',
    icon: faUser,
    component: Users,
    isAdmin: true,
  },
  {
    path: '/teachers',
    sidebarName: 'المعلمين',
    icon: faUniversalAccess,
    component: Teachers,
    isAdmin: true,
  },
  {
    path: '/feedbacks',
    sidebarName: 'الاقتراحات',
    icon: faEnvelope,
    component: Feedbacks,
    isAdmin: true,
  },
  { redirect: true, path: '/mathwords' }
]
