import React from 'react';
import { reduxForm } from 'redux-form';

const formHoc = (config = {}) => WrapedComponent => {
  const Form = (props) => {
    return (
      <WrapedComponent {...props} />
    )
  }

  return reduxForm({
    form: WrapedComponent.name || 'form',
    ...config,
  })(Form);
}

export default formHoc;
