import { combineReducers } from 'redux'
import creatCRUDReducer from './CRUD/reducer';
import loginReducer from './login/reducer';
import modalReducer from './modal/reducer';
import { reducer as fromReducer } from 'redux-form';
import { i18nReducer } from 'react-redux-i18n';

export default combineReducers({
  form: fromReducer,
  i18n: i18nReducer,
  auth: loginReducer,
  modal: modalReducer,
  users: creatCRUDReducer('users'),
  teachers: creatCRUDReducer('teachers'),
  mathwords: creatCRUDReducer('mathwords'),
  applications: creatCRUDReducer('applications'),
  testsites: creatCRUDReducer('testsites'),
  suggestion_links: creatCRUDReducer('suggestion_links'),
  feedbacks: creatCRUDReducer('feedbacks'),
})
