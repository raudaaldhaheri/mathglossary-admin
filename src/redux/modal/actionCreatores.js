
export const toggleModal = () => ({ type: 'TOGGLE_MODAL' })
export const showSignin = payload => ({ type: 'TOGGLE_SIGN_IN_UP_FORM', payload })