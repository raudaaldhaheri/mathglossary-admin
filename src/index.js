import React from 'react';
import ReactDOM from 'react-dom';
import { loadTranslations, setLocale, syncTranslationWithStore } from 'react-redux-i18n';
import { ThemeProvider } from 'styled-components';
import { Provider } from 'react-redux';
import { HashRouter as Router } from 'react-router-dom';
import App from './App';
import translationsObject from './translations';
import * as serviceWorker from './serviceWorker';
import { PersistGate } from 'redux-persist/integration/react';
import { ToastContainer } from 'react-toastify';

import createStore from 'redux/store';
import theme from 'layouts/themeConfig';
import GlobalStyle from 'layouts/themeConfig/GlobalStyle';
import 'react-widgets/dist/css/react-widgets.css';
import 'react-toastify/dist/ReactToastify.css';


const { store, persistor } = createStore()
syncTranslationWithStore(store)
store.dispatch(loadTranslations(translationsObject));
store.dispatch(setLocale('ar'));

ReactDOM.render(
  <Provider store={store} >
    <PersistGate loading={'...'} persistor={persistor} >
      <ThemeProvider theme={theme} >
        <React.Fragment>
          <GlobalStyle />
          <ToastContainer />
          <Router>
            <App />
          </Router>
        </React.Fragment>
      </ThemeProvider>
    </PersistGate>
  </Provider>
, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.register();
