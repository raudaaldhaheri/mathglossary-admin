import React from 'react';
import { Field } from 'redux-form';
import { modelName } from '../../config';
import styled from 'styled-components';
import LabledData from 'components/LabledData';
import formHoc from 'hocs/formHoc';
import Input from 'components/Input';
import Textarea from 'components/Textarea';
import FlexBox from 'components/FlexBox';
import { Button } from 'components/Buttons/Button';
import {
  isRequired,
} from 'components/redux-form-validations';

const StyledDiv = styled.div`
  padding: 10px 20px;
  margin-bottom: 30px;
  border-radius: 20px;
  border: 1px solid #ccc;
  text-align: center;
`;

const textApiConfig = [
  {
    title: 'المصطلح بلعربي',
    validate: [isRequired],
    key: 'application_ar',
  },
  {
    title: 'المصطلح بلأنحليزي',
    validate: [isRequired],
    key: 'application_en',
  },
  // {
  //   title: 'الرسم التطبيقي للمصطلح بلأنحليزي',
  //   key: 'application_diagram_en',
  // },
  // {
  //   title: 'الرسم التطبيقي للمصطلح بلعربي',
  //   key: 'application_diagram_ar',
  // },
  {
    title: 'معرف الفيديو من اليوتيوب للمصطلح بلعربي',
    key: 'application_video_en',
  },
  {
    title: 'معرف الفيديو من اليوتيوب للمصطلح إنجليزي',
    key: 'application_video_ar',
  },
  {
    title: 'الشرح التطبيقي للمصطلح بلعربي',
    component: Textarea.Field,
    key: 'application_description_ar',
  },
  {
    title: 'الشرح التطبيقي للمصطلح بلأنحليزي',
    component: Textarea.Field,
    key: 'application_description_en',
  },
];

const Form = ({ handleSubmit, submitting, ...props }) => (
  <form {...props} onSubmit={handleSubmit}>
    <StyledDiv>
      <p>
        معرف فيديو اليوتيوب عبارة عن نص في اخر رابط المقطع مثال
      </p>
      <p>www.youtube.com/watch?v=<strong>fwMwuiIahn8</strong></p>
      <p>الخط العريض هو معرف الفيديو في المثال</p>
    </StyledDiv>
    النص بالخط العريض عبارة عن معرف الفيديو
    <FlexBox>
      {textApiConfig.map(({ title, key, validate, ...props }) => <LabledData
        lable={title}
        data={
          <Field
            component={Input.Field}
            {...props}
            name={key}
            placeholder={`اكتب ${title}...`}
            validate={validate}
          />
        }
      />)}
    </FlexBox>
    <Button fixedSize disabled={submitting} onClick={handleSubmit}>
      تاكيد
    </Button>
  </form>
);

export default formHoc({ form: `${modelName}-from` })(Form);
