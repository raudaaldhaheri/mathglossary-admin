import React from 'react';
// import PropTypes from 'prop-types';
import { LinkButton } from 'components/Buttons';
import queryString from 'qs'
import { modelName } from '../config';
import { CirecleButton } from 'components/Buttons';
import AwesomeTable from 'components/AwesomeTable';
import TableWrapper from 'components/TableWrapper';
import { faPencilAlt, faTimes, faSpinner } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { toast } from 'react-toastify';

class List extends React.Component {
  state = {
    openModal: false,
  }
  componentDidMount () {
    this.props.getRecords().catch(this.handleError);
  };

  componentDidUpdate(prevProps) {
    const prevSearch = queryString.parse(prevProps.location.search)['?search']
    const currentSearch = queryString.parse(this.props.location.search)['?search']
    if (prevSearch && prevSearch !== currentSearch) {
      this.props.getRecords({ search: currentSearch });
    }
  }

  toggleModal = () => {
    this.setState({ openModal: !this.state.openModal });
  }

  handleError = ({ msg }) => toast.error(msg)
  handleDelete = (id) => {
    this.props.deleteRecord(id)
  }

  headers = [
    {
      title: 'المصطلح بلعربي',
      key: 'application_ar',
    },
    {
      title: 'المصطلح بلأنحليزي',
      key: 'application_en',
    },
    {
      title: 'شرح المصطلح بلعربي',
      key: 'application_description_ar',
    },
    {
      title: 'شرح المصطلح بلأنحليزي',
      key: 'application_description_en',
    },
    {
      title: 'معرف اليوتيوب العربي',
      key: 'application_video_ar',
    },
    {
      title: 'معرف اليوتيوب الانجليزي',
      key: 'application_video_en',
    },
    {
      title: 'العمليات',
      render: (record) => (
        <React.Fragment>
          <CirecleButton
            icon={faTimes}
            background="ganger"
            onClick={() => this.handleDelete(record.id)}
          />
          <CirecleButton
            icon={faPencilAlt}
            background="warn"
            color="light"
            onClick={() => this.props.initlizeRecord(record)}
            to={`/${modelName}/${record.id}/edit`}
          />
        </React.Fragment>
      ),
    },
  ]

  render() {
    const { fetching, list } = this.props[modelName];
    return (
      <div>
        <LinkButton to={`/${modelName}/new`} >
          إضافة
        </LinkButton>
        {fetching && <FontAwesomeIcon icon={faSpinner} pulse />}
        <TableWrapper>
          <AwesomeTable headers={this.headers} data={list} />
        </TableWrapper>
      </div>
    )
  }
};

List.propTypes = {

}

export default List;
