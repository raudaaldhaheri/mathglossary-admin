import React from 'react';
import { connect } from 'react-redux';
import { Switch, Route } from 'react-router-dom';
import { modelName } from './config';

import actionCreators from 'redux/CRUD/actionCreators';

import List from './List';
import Edit from './Edit';

const providersActions = actionCreators(modelName);

const mapStateToProps = (state) => ({
  [modelName]: state[modelName]
})

const mapDispatchToProps = dispatch => ({
  getRecords: (params) => dispatch(providersActions.getRecords(params)),
  getRecord: (params) => dispatch(providersActions.getRecord(params)),
  deleteRecord: (params) => dispatch(providersActions.deleteRecord(params)),
  updateRecord: (params) => dispatch(providersActions.updateRecord(params)),
  initlizeRecord: (params) => dispatch(providersActions.initlizeRecord(params)),
})

const ConnectedList = connect(mapStateToProps, mapDispatchToProps)(List)
const ConnectedEdit = connect(mapStateToProps, mapDispatchToProps)(Edit)

const Router = () => {
  return (
    <Switch>
      <Route
        exact
        path={`/${modelName}`}
        component={ConnectedList}
      />
      <Route
        exact
        path={`/${modelName}/:id/edit`}
        component={ConnectedEdit}
      />
    </Switch>
  )
}

export default Router;
