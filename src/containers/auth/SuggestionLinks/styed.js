import styled from 'styled-components';

export const Div = styled.div`
  display: flex;
  flex-wrap: wrap;
  flex: 1 1 300px;
  justify-content: space-between;
`