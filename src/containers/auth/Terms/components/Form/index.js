import React from 'react';
import { Field } from 'redux-form';
import { modelName } from '../../config';

import LabledData from 'components/LabledData';
import formHoc from 'hocs/formHoc';
import Input from 'components/Input';
import FlexBox from 'components/FlexBox';
import { Button } from 'components/Buttons/Button';
import {
  isRequired,
  isUrl,
} from 'components/redux-form-validations';

const textApiConfig = [
  {
    title: 'المصطلح بلعربي',
    validate: [isRequired],
    key: 'term_ar',
  },
  {
    title: 'المصطلح بلأنحليزي',
    validate: [isRequired],
    key: 'term_en',
  },
  {
    title: 'رابط الشرح',
    validate: [isRequired, isUrl],
    key: 'link',
  },
  {
    title: 'شرح المصطلح بلعربي',
    validate: [isRequired],
    key: 'term_description_ar',
  },
  {
    title: 'شرح المصطلح بلأنحليزي',
    validate: [isRequired],
    key: 'term_description_en',
  },
];

const Form = ({ handleSubmit, submitting, ...props }) => (
  <form {...props} onSubmit={handleSubmit}>
    <FlexBox>
      {textApiConfig.map(({ title, key, validate, ...props }) => <LabledData
        lable={title}
        data={
          <Field
            {...props}
            name={key}
            placeholder={`اكتب ${title}...`}
            component={Input.Field}
            validate={validate}
          />
        }
      />)}
    </FlexBox>
    <Button fixedSize disabled={submitting} onClick={handleSubmit}>
      تاكيد
    </Button>
  </form>
);

export default formHoc({ form: `${modelName}-from` })(Form);
