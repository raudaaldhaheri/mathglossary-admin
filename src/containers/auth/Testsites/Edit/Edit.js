import React from 'react';
import { toast } from 'react-toastify';
import Form from '../components/Form';
import { modelName } from '../config';

const Edit = ({ updateRecord, history, ...props  }) => {
  const handleSubmit = (data) => {
    console.log({data})
    return updateRecord(data).then(handleSuccess).catch(handleError);
  };

  const handleSuccess = ({ data }) => {
    toast.success(data.message)
    history.push(`/${modelName}`);
  }
  const handleError = ({ msg }) => toast.error(msg)

  return (
    <div>
      <Form onSubmit={handleSubmit} initialValues={props[modelName].details}  />
    </div>
  )
}

Edit.propTypes = {

}

export default Edit;
