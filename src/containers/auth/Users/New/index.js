import React from 'react'
import { Div } from '../styed';
import { modelName } from '../config';
import Form from '../components/Form';
import { toast } from 'react-toastify';

const New = (props) => {
  const handleSubmit = (data) => {
    return props.createRecord(data).then(handleSuccess).catch(handleError);
  }
  const handleSuccess = ({ data }) => {
    toast.success(data.message)
    props.history.push(`/${modelName}`);
  }
  const handleError = ({ msg }) => toast.error(msg)
  return (
    <React.Fragment>
      <Div>
        <Form onSubmit={handleSubmit} />
      </Div>
    </React.Fragment>
  )
}

New.propTypes = {

}

export default New;
