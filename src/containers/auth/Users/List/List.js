import React from 'react';
// import PropTypes from 'prop-types';
import { LinkButton } from 'components/Buttons';
import queryString from 'qs'
import { modelName } from '../config';
import { CirecleButton } from 'components/Buttons';
import AwesomeTable from 'components/AwesomeTable';
import TableWrapper from 'components/TableWrapper';
import { faPencilAlt, faTimes, faSpinner } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { toast } from 'react-toastify';

class List extends React.Component {
  state = {
    openModal: false,
  }
  componentDidMount () {
    this.handleGetRecords()
  };

  componentDidUpdate(prevProps) {
    const prevSearch = queryString.parse(prevProps.location.search)['?search']
    const currentSearch = queryString.parse(this.props.location.search)['?search']
    if (prevSearch && prevSearch !== currentSearch) {
      this.props.getRecords({ search: currentSearch });
    }
  }

  toggleModal = () => {
    this.setState({ openModal: !this.state.openModal });
  }

  handleGetRecords = () => this.props.getRecords();
  handleDelete = (id) => {
    this.props.deleteRecord(id).then(this.handleDeleted)
  }

  handleDeleted = (response) => {
    toast.success('');
    this.handleGetRecords()
  }

  headers = [
    {
      title: 'الاسم',
      key: 'name',
    },
    {
      title: 'العمليات',
      render: (record) => (
        <React.Fragment>
          <CirecleButton
            icon={faTimes}
            background="ganger"
            onClick={() => this.handleDelete(record.id)}
          />
          <CirecleButton
            icon={faPencilAlt}
            background="warn"
            color="light"
            onClick={() => this.props.initlizeRecord(record)}
            to={`/${modelName}/${record.id}/edit`}
          />
        </React.Fragment>
      ),
    },
  ]

  render() {
    const { fetching, list } = this.props[modelName];
    return (
      <div>
        <LinkButton to={`/${modelName}/new`} >
          إضافة
        </LinkButton>
        {fetching && <FontAwesomeIcon icon={faSpinner} pulse />}
        <TableWrapper>
          <AwesomeTable headers={this.headers} data={list} />
        </TableWrapper>
      </div>
    )
  }
};

List.propTypes = {

}

export default List;
