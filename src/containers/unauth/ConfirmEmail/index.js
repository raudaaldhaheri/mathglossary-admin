import React from 'react'
import Api from 'Api';

import Confirm from './components/Confirm';
import { toast } from 'react-toastify';
import { I18n } from 'react-redux-i18n';

class ConfirmEmail extends React.Component {
  state = {isConfiremd: false, fetching: false}
  handleSubmit = () => {
    
    const { email_confirmation_token } = this.props.match.params;
    this.setState({fetching: true}, () => Api.post('users/confirm_email', { email_confirmation_token })
    .then(this.handleSuccess)
    .catch(this.handleError)
    )
  }
  handleError = () => this.setState({ fetching: false }, () => toast.error(I18n.t('no_user_has_this_token')));
  handleSuccess = () => this.setState({ fetching: false, isConfiremd: true });

  render() {
    const { isConfiremd, fetching } = this.state;
    return (
      isConfiremd ? <div>{I18n.t('user_cofirmed')}</div> :
      <Confirm fetching={fetching} onSubmit={this.handleSubmit} />
    );
  }
}

export default ConfirmEmail;
