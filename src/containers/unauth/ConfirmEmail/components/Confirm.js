import React from 'react'
import styled from 'styled-components'
import { I18n } from 'react-redux-i18n';

const StyledSpan = styled.span`
  border-bottom: 1px dotted #9e9e9e;
  cursor: pointer;
  opacity: ${({disabled}) => disabled && '0.5'};
  :hover {
    border-bottom-style: solid; 
  }
`
const Confirm = ({onSubmit, fetching}) => (
  <div>
    <h2>{I18n.t('confirm_email')}</h2>
    <StyledSpan disabled={fetching} onClick={onSubmit} role="button">{I18n.t('click_me')}</StyledSpan>
  </div>
)

export default Confirm;
