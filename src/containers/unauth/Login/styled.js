import styled from 'styled-components';
import { Button } from 'components/Buttons';

export const StyledButton = styled(Button)`
  display: block;
  width: 100%;
  margin-top: 2rem;
`;

export const LoginForm = styled.form`
  padding: 1rem;
  border: 1px solid #eee;
  border-radius: ${({ theme }) => theme.sizes.radius};
`;
