import React from 'react';
import PropTypes from 'prop-types';
import Input from 'components/Input';
import formHoc from 'hocs/formHoc';
import { Field } from 'redux-form';
import { LoginForm, StyledButton as Button } from './styled';
import {
  isEmail,
} from 'components/redux-form-validations';

const required = value => (value ? undefined : 'Required');

const RestForm = ({handleSubmit}) => {
  return (
    <form onSubmit={handleSubmit}>
      <Field
        type="email"
        name="email"
        component={Input.Field}
        validate={[required, isEmail]}
        placeholder="اكتب ايميلك"
      />
      <Button type="submit">تاكيد</Button>
    </form>
  )
}

export const ResetPasswordForm = formHoc()(RestForm);

const Login = ({
  handleSubmit,
  fetching,
  resetPassword,
  ...props
}) => (
  <React.Fragment>
    <LoginForm  {...props} netlify netlify-honeypot="bot-field" onSubmit={handleSubmit}>
      <Field
        name="email"
        component={Input.Field}
        placeholder="Name"
        validate={required}
      />
      <Field
        name="password"
        component={Input.Field}
        validate={required}
        type="password"
        placeholder="Password"
      />
      <Button
        onClick={handleSubmit}
        style={{ with: '100%' }}
        fetching={fetching}
      >
        تاكيد
      </Button>
    </LoginForm>
    <small role="button" onClick={resetPassword}>نسيان كلمة المرور</small>
  </React.Fragment>
);

Login.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  fetching: PropTypes.bool.isRequired,
};

export default formHoc({
  form: 'LoginForm'
})(Login);
