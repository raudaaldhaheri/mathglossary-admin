import React from 'react';
import Form, { ResetPasswordForm } from './Form';
import { signin } from 'redux/login/actionCreators';
import { connect } from 'react-redux';
import Modal from 'components/Modal';
import { toast } from 'react-toastify';

class LoginForm extends React.Component {
  state = {
    isOpen: false,
  }
  handleSubmit = (data) => {
    const { hitLogin } = this.props
    hitLogin(data).catch(this.handleError)
  }

  handleError = ({ msg }) => toast.error(msg);

  toggel = () => this.setState({ isOpen: !this.state.isOpen })

  render() {
    return (
      <React.Fragment>
        <Modal
          isOpen={this.state.isOpen}
          onClose={this.toggel}
        >
          <ResetPasswordForm onSubmit={this.resetPassword} />
        </Modal>
        <Form {...this.props} resetPassword={this.toggel} onSubmit={this.handleSubmit} />
      </React.Fragment>
    );
  }

}

const mapDispatchToProps = dispatch => ({
  hitLogin: user => dispatch(signin(user)),
});

const mapStateToProps = ({ auth: { fetching } }) => ({
  fetching,
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginForm);
