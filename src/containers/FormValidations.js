import moment from 'moment';
import { I18n } from 'react-redux-i18n';

export const required = value => (value ? undefined : 'Required');
export const maxLength = max => value => (
  value && value.length > max ? I18n.t('max_length', { max }) : undefined
);
export const maxLength15 = maxLength(15);
export const minLength = min => value => (
  value && value.length < min ? I18n.t('min_length', { min }) : undefined
);
export const minLength2 = minLength(2);
export const number = value => (
  value && (Number.isNaN(Number(value)) || /\D/.test(value))
    ? I18n.t('number_msg')
    : undefined
);
export const minValue = min => value => (
  value && value < min ? I18n.t('min_value_msg') : undefined
);
export const minValue18 = minValue(18);
export const email = value => (
  value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,10}$/i.test(value)
    ? I18n.t('email_msg')
    : undefined
);
export const tooOld = value => (
  value && value > 65 ? I18n.t('too_old_msg') : undefined
);
export const aol = value => (
  value && /.+@aol\.com/.test(value)
    ? I18n.t('aol_msg')
    : undefined
);
export const alphaNumeric = value => (
  value && /[^a-zA-Z0-9 ]/i.test(value)
    ? I18n.t('alpha_numeric_msg')
    : undefined
);
export const phoneNumber = value => (
  value && !/^(0|[1-9][0-9]{9})$/i.test(value)
    ? I18n.t('phone_number_msg')
    : undefined
);

export const date = value => (
  value && moment(value).isValid() ? undefined : I18n.t('invalid_date_msg')
);

export const hijriDate = (value) => {
  const GEO_DATE = moment(value, 'iYYYY-iMM-iDD').format('YYYY-MM-DD');
  return (
    value && moment(GEO_DATE).isValid() ? undefined : I18n.t('invalid_hijri_date_msg')
  );
};
