export const normlizeNumber = value => value && value.replace(/[^\d.]/g, '');

export const normlizeMobile = value => {
  if (!value) return 5;
  const normlized = normlizeNumber(value);
  return normlized;
};
