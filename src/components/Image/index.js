import { Image } from './Image';
import { ImageField } from './ImageField';

Image.Field = ImageField;

export default Image;
