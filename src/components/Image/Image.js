import React from 'react';
import styled from 'styled-components';
import yemenLogo from 'assets/img/yemen.svg.png';

const ImageContainer = styled.div`
  width: 150px;
  height: 150px;
  border-radius: 150px;
  border: 1px solid #ccc;
  overflow: hidden;
  display: flex;
  align-items: center;
  justify-content: center;
  > img {
    width: 100%;
  }
`

export const Image = ({ src, alt, ...props }) => (
  <ImageContainer>
    <img {...props} src={src || yemenLogo} alt={alt} />
  </ImageContainer>
);
