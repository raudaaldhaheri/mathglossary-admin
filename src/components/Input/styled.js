import styled from 'styled-components';

export const Input = styled.input`
  border: 1px solid #ccc;
  border-radius: ${({ theme }) => theme.sizes.radius};
  height: 2rem;
  padding: 0 1rem;
  background-color: transparent;
  box-shadow: inset 0 1px 1px rgba(0,0,0,0.075);
  width: 100%;
  height: 2.7em;
  box-sizing: border-box;
`
