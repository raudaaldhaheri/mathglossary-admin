import { Input } from './styled';
import InputField from './InputField';

Input.Field = InputField;

export { InputField };
export default Input;
