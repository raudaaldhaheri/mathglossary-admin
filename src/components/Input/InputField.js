import React from 'react'
import Input from './index';
import { ErrorMessage } from 'components/Massage';

export default ({
  input: {
    onChange,
    ...input,
  },
  meta: {
    invalid,
    error,
    touched,
  },
  style,
  ...props
}) => {
  return (
    <div style={style}>
      <Input
        {...props}
        {...input}
        onChange={value => onChange(value)}
      />
      {touched && invalid && error && <ErrorMessage>{error}</ErrorMessage>}
    </div>
  )
}
