import styled from 'styled-components';

const Wrapper = styled.div`
  max-width: 1000px;
  margin: auto;
  flex: 1;
  ${({ flex }) => flex && 'display: flex;'}
  ${({ between }) => between && 'justify-content: space-between;'} 
`;

export default Wrapper;