import styled from 'styled-components';

export const Wrapper = styled.div`
  width: 100%;
  border: 1px solid ${props => props.theme.colors.light};
  border-radius: ${({ theme }) => theme.sizes.radius};
  overflow: auto;

  table {
    width: 100%;
    border: none;
    border-collapse: collapse;
  }
  tr {
    :nth-child(even) {
      background: ${props => props.theme.colors.light};
    }
  }
  td,
  th {
    padding: 10px;
    text-align: center;
    vertical-align: middle;
  }
  th {
    border-bottom: 1px solid ${({ theme }) => theme.colors.light}
  }
`