import React from 'react'
import PropTypes from 'prop-types'
import { Box } from './styled';

const FlexBox = ({
  justifyContent,
  alignItems,
  ...props
}) => (
  <Box
    {...props}
    justifyContent={justifyContent}
    alignItems={alignItems}
  />
)

const justifyContentProps = [
  'center',
  'start',
  'end',
  'flex-start',
  'flex-end',
  'left',
  'right',
  'space-between',
  'space-around',
  'space-evenly',
  'stretch',
  'safe center',
  'unsafe center',
  'inherit',
  'initial',
  'unset',
];

const alignItemsProps = [
  'center',
  'start',
  'end',
  'flex-start',
  'flex-end',
  'self-start',
  'self-end',
];

FlexBox.propTypes = {
  justifyContent: PropTypes.oneOf(justifyContentProps),
  alignItems: PropTypes.oneOf(alignItemsProps)
}

export default FlexBox;
