import styled from 'styled-components';

export const Box = styled.div`
  display: flex;
  width: 100%;
  flex-wrap: wrap;
  justify-content: ${props => props.justfiyContent || 'space-evenly'};
  align-items: ${props => props.aligItems};
  &> * {
    flex: 1 1 300px;
    margin: 10px;
  }
`
