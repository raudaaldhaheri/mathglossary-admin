import DropdownField from './DropdownField';
import CreatableDropdown from './CreatableDropdown';
import Dropdown from './Dropdown';

Dropdown.Field = DropdownField;
Dropdown.Creatable = CreatableDropdown;

export default Dropdown;
