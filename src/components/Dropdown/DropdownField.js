import React from 'react'
import Dropdown from './Dropdown';
import { ErrorMessage } from 'components/Massage';

export default ({
  input: {
    onChange,
    ...input,
  },
  meta: {
    invalid,
    error,
    touched,
  },
  valueField,
  ...props
}) => {
  const handleChange = (val) => {
    const payload = val[valueField] || val;
    onChange(payload)
  }

  return (
    <React.Fragment>
      <Dropdown {...props} {...input} valueField={valueField} onChange={handleChange} />
      <div>
        {touched && invalid && error && <ErrorMessage>{error}</ErrorMessage>}
      </div>
    </React.Fragment>
  )
}
