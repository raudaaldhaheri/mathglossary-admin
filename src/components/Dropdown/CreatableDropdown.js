import React from 'react';
import Api from 'Api';
import Dropdown from './DropdownField';

class CreatableDropdown extends React.Component {
  state = { list: [], fetching: false }
  componentDidMount() {
    this.setState({ fetching: true })
    this.getList();
  }

  getList = () => Api.get(this.props.endPoint).then(this.handleSuccess).catch(this.handleError);
  handleSuccess = ({ data }) => this.setState({ fetching: false, list: data[this.props.endPoint] });
  handleError = ({ msg }) => this.setState({ fetching: false, error: msg });

  render() {
    const { fetching, list, error } = this.state;
    return (
      <div>
        <Dropdown
          {...this.props}
          fetching={fetching}
          options={list}
        />
        {error && <small>{error}</small>}
      </div>
    );
  }
}

export default CreatableDropdown;
