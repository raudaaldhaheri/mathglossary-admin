import React from 'react';

import { DropdownList } from 'react-widgets'

const Dropdown = ({
  options,
  fetching,
  ...props
}) => (
  <DropdownList
    {...props}
    busy={fetching}
    data={options}
  />
);

export default Dropdown;
