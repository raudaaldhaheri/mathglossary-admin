import styled from 'styled-components';

export const Input = styled.select`
  border: 1px solid #eee;
  border-radius: ${({ theme }) => theme.sizes.radius};
  height: 2rem;
  padding: inherit 1rem;
  width: 100%;
  line-height: 2rem;
`
