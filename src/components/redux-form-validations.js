
import { required, email, numericality, length, url } from 'redux-form-validators';
import { I18n } from 'react-redux-i18n';

export const isUrl = url({ protocols: ['http', 'https'] })
export const isRequired = required({ msg: 'هذا الحقل مطلوب', });
export const isMobile = numericality({ msg: I18n.t('رقم الجوال مثل 00965258789'), });
export const isNumber = numericality({ msg: I18n.t('رقم'), });
export const isEmail = email({ msg: I18n.t('صيغة البريد غير صحيحة'), });
export const maxLength = max => length({ max, message: 'الرقم طويل لا يتعدا طول الرقم عشرين رقم' });
