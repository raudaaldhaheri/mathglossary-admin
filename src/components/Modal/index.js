import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Content from './Content';

export default class Modal extends Component {
  static propTypes = {
    isOpen: PropTypes.bool.isRequired,
  }

  constructor(props) {
    super(props)

    this.outer = React.createRef();
    this.state = { showModal: props.isOpen };
  }

  componentDidMount () {
    window.addEventListener('click', this.handleClick);
  }

  componentDidUpdate (prevProps) {
    if (prevProps.isOpen !== this.props.isOpen) {
      this.handleToggleModal(this.props.isOpen)
    }
  }

  componentWillUnmount () {
    clearTimeout(this.timer)
  }
  
  handleClick = (event) => {
    if (event.target === this.outer.current) {
      this.props.onClose();
    }
  }

  handleToggleModal = (isOpen) => {
    if (isOpen) {
      return this.setState({ showModal: this.props.isOpen });
    };
    this.timer = setTimeout(() => {
      this.setState({ showModal: this.props.isOpen })
    }, 100)
  }
  
  handleClose = () => {
    this.props.onClose();
  }

  render() {
    const { showModal } = this.state;
    return (
      showModal ?
        <Content outerRef={this.outer} {...this.props} /> :
        null
    )
  }
}
