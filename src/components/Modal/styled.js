import styled from 'styled-components';

export const Outer = styled.div`
  background-color: rgba(0,0,0, .8);
  overflow: auto;
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  z-index: 1;
`;

export const CloseButton = styled.small`
  cursor: pointer;
  display: inline-block;
  height: 30px;
  width: 30px;
  line-height: 30px;
  text-align: center;
  margin: 10px 0 50px;
  border: 1px solid #fff;
`;

export const Inner = styled.div`
  max-width: 400px;
  background-color: #fff;
  padding: 10px 30px;
  color: #fff !important;
  display: flex;
  box-sizing: border-box;
  height: 100vh;
  background: linear-gradient(45deg, rgba(0,0,0, .9), rgba(232,37,16,.8));

  small { color: #fff; }
  input {
    color: #fff;
    &::placeholder {
      color: #fff;
    }
  }
`;
