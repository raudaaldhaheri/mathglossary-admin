import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { Outer, Inner, CloseButton } from './styled';

const portalcreateElement = () => {
  const portal = document.createElement('div');
  portal.setAttribute('id', 'modal');
  document.body.appendChild(portal);
  return portal;
}

const portal = document.getElementById('modal') || portalcreateElement();

const Content = ({
  onClose,
  outerRef,
  children,
  closeButton,
  title,
}) => {
  return ReactDOM.createPortal(
    <Outer ref={outerRef} className="animated fadeIn">
      <Inner className="animated fadeInDown">
        <div style={{ flex: 1 }}>
          <CloseButton role="button" onClick={onClose}>X</CloseButton>
          {children}
        </div>
      </Inner>
    </Outer>,
    portal,
  )
}

Content.propTypes = {
  closeButton: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.node,
    PropTypes.element,
  ]),
  title: PropTypes.string,
  onClose: PropTypes.func.isRequired,
}

export default Content
