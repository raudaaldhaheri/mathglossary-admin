import styled from 'styled-components';

const Card = styled.div`
  width: 30rem;
  background: #fff;
  border-radius: 10px;
  padding: 10px;
  border: 1px solid #eee;
  text-aign: center;
`
const CardHeader = styled.div`
  display: flex;
  width: 100%;
  font-wigth: bold;
  padding-bottom: 10px;
  margin-bottom: 10px;
  border-bottom: 1px solid #eee; 
  justify-content: space-between;
`

const CardItem = styled.div`
  display: flex;
  width: 100%;
  padding-bottom: 10px;
  margin-bottom: 10px;
  text-align: center;
`

Card.Header = CardHeader;
Card.Item = CardItem;

export default Card;

