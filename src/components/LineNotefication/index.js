import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import Content from './Content';

export default class LineNotefication extends PureComponent {
  static propTypes = {
    isOpen: PropTypes.bool.isRequired,
  }

  constructor(props) {
    super(props)

    this.outer = React.createRef();
    this.state = { showModal: props.isOpen };
  }

  componentDidMount () {
    window.addEventListener('click', this.handleClick);
    this.handleToggleModal(this.props.message);
  }

  componentWillReceiveProps(props) {
    if (props.message) {
      this.handleToggleModal(props.message)
    }
  }

  componentWillUnmount () {
    clearTimeout(this.timer)
  }

  handleClick = (event) => {
    if (event.target === this.outer.current) {
      this.props.onClose();
    }
  }

  handleToggleModal = (isOpen) => {
    if (isOpen) {
      this.setState({ showModal: true });
      this.closeTimer = setTimeout(() => this.setState({ showModal: false }), 5000)
    };
  }

  handleClose = () => {
    this.props.onClose();
  }

  render() {
    const { showModal } = this.state;
    return (
      showModal ?
        <Content outerRef={this.outer} {...this.props} /> :
        null
    )
  }
}
