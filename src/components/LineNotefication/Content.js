import React from 'react';
import ReactDOM from 'react-dom';
import { LinerWrapper } from './styled';

const makeid = () => {
  let text = "";
  const possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

  for (let i = 0; i < 5; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
}

const portalcreateElement = () => {
  const portal = document.createElement('div');
  portal.setAttribute('id', `liner-message-${makeid()}`);
  document.body.insertAdjacentElement('afterbegin', portal);
  return portal;
}

const portal = portalcreateElement();

const Content = ({
  message,
}) => ReactDOM.createPortal(
  <LinerWrapper>{message}</LinerWrapper>
  ,portal
);

Content.propTypes = {

}

export default Content;
