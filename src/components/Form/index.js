import styled from 'styled-components';

const StyledForm = styled.form`
  display: block;
  width: 100%;
  box-sizing: border-box;
`;

export default StyledForm;
