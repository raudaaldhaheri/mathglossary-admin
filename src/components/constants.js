import * as themeConfig from 'layouts/themeConfig';

export const COLORS = Object.keys(themeConfig.COLORS).map(key => key);
