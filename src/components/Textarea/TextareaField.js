import React from 'react'
import TextareaField from './index';
import { ErrorMessage } from 'components/Massage';

export default ({
  input: {
    onChange,
    ...input,
  },
  meta: {
    invalid,
    error,
    touched,
  },
  style,
  children,
  ...props
}) => {
  return (
    <div style={style}>
      <TextareaField
        {...props}
        {...input}
        onChange={value => onChange(value)}
      >
        {children}
      </TextareaField>
      {touched && invalid && error && <ErrorMessage>{error}</ErrorMessage>}
    </div>
  )
}
