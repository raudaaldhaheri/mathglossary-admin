import styled from 'styled-components';

export const Textarea = styled.textarea`
  border-radius: ${({ theme }) => theme.sizes.radius};
  padding: 0 1rem;
  background-color: transparent;
  box-shadow: inset 0 1px 1px rgba(0,0,0,0.075);
  width: 100%;
  height: 200px;
  box-sizing: border-box;
`
