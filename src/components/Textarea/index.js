import { Textarea } from './styled';
import TextareaField from './TextareaField';

Textarea.Field = TextareaField;

export { TextareaField };
export default Textarea;
