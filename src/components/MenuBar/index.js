import { connect } from 'react-redux';
import { toggleModal, showSignin } from 'redux/modal/actionCreatores';
import MenuBar from './MenuBar';

const mapDispatchToProps = dispatch => ({
  toggleModal: () => dispatch(toggleModal()),
  toggleSigninModal: () => dispatch(showSignin(true)),
  toggleSignupModal: () => dispatch(showSignin(false)),
  logout: () => dispatch({ type: 'LOGOUT' }),
});

const mapStateToProps = ({ auth: { token }}) => ({ token })

export default connect(mapStateToProps, mapDispatchToProps)(MenuBar);
