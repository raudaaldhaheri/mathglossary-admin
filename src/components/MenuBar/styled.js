import styled from 'styled-components';

export const SigninBtn = styled.button`
  border: none;
  padding: 5px 10px;
  font-weight: 500;
  cursor: pointer;
  ${({ primary }) => `
    background:  ${primary ? 'linear-gradient(45deg, rgba(0,0,0, .9), rgba(232,37,16,.8))' : '#fff'};
    color: ${primary && '#fff'}
    border-radius: ${primary && '5px'}
  `}
`

export const MenuBar = styled.div`
  height: 40px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  border-bottom: 1px solid #ccc;
  margin-bottom: 50px;
`