import styled from 'styled-components';
import { Link } from 'react-router-dom';

export const LinkButton = styled(Link)`
  border-radius: ${({ theme }) => theme.sizes.radius};
  text-decoration: none;
  background-color: ${({ theme}) => theme.colors.primary};
  color: #fff;
  opacity: .8;
  padding: 0 5px;
  margin: 2px;
  text-align: center;
  max-width: 100px;
  display: block;

  :hover {
    opacity: 1;
    color: #fff;
  }
`
