import styled from 'styled-components';

export const ButtonWrapper = styled.button`
  border: 0;
  border-radius: 100%;
  background: ${({ background, theme }) => theme.colors[background]};
  color: ${({ color, theme }) => theme.colors[color]};
  width: 2rem;
  opacity: .78;
  height: 2rem;
  overflow: hidden;
  margin: 0 4px;
  box-shadow: 0px 10px 10px #ccc;

  &:hover {
    box-shadow: 0px 4px 10px #ccc;
    opacity: 1;
  }
`;
