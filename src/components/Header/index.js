import React from 'react';
import _ from 'lodash';
import { withRouter } from 'react-router-dom';
import { Div, StyledLink, SearchWrapper } from './styled';
import { Input } from 'components/Input/styled';
import { I18n } from 'react-redux-i18n';
import pluralize from 'pluralize';

const renderTabs = (urlString) => {
  const path = urlString.split('/');
  if (_.isEmpty(path)) return urlString;
  let link = '';
  return path.map((tab, key) => {
    const isTabNumber = !!(parseInt(tab))
    if (_.isEmpty(tab)) return null;
    if (key === path.length - 1) {
      if (isTabNumber) return 'التفاصيل';
      return I18n.t(tab)
    };
    const showIcon = !!(key > 0);
    link = `${link}/${tab}`;
    return (
      <React.Fragment>
        <StyledLink to={link} >
        {isTabNumber || I18n.t(tab)}
        </StyledLink>
        {showIcon && ' > '}
      </React.Fragment>
    )
  })

};

const Header = ({
  title,
  rightLayout,
  location: { pathname, ...location },
  history,
  ...rest
}) => {
  const handleTyping = ({ target }) => {
    history.push({
      search: `?search=${target.value}`,
    })
  }
  const lastIsList = pluralize.isPlural(pathname.split('/').pop());
  return(
    <React.Fragment>
      <SearchWrapper>
        <div>
          <h2>{renderTabs(pathname)}</h2>
        </div>
        {lastIsList && <Input onChange={handleTyping} style={{ maxWidth: 300 }} type="search" placeholder="عن ماذا تبحث؟" />}
      </SearchWrapper>
      <Div bordred style={{ marginBottom: 40 }}>
        {rightLayout}
      </Div>
    </React.Fragment>
  );
};

export default withRouter(Header);