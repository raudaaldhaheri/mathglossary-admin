import styled from 'styled-components';
import { Link } from 'react-router-dom';

export const Div = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  justify-content: space-between;
  border-bottom: ${props => props.bordred && '3px solid #fff'};
`;

export const SearchWrapper = styled(Div)`
  @media screen and (max-width: 450px) {
    margin-top: 5rem;  
  }
`;

export const StyledLink = styled(Link)`
  font-size: 1.5rem;
  color: #555;
  text-decoration: none;
  
  &:hover {
    color: ${({ theme }) => theme.colors.info}
  }
`;
