import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import Authenticated from 'layouts/auth'
import UnAuthenticated from 'layouts/unauth'
import Api from 'Api';
// import 'animate.css';

const App = ({ token }) => {
  Api.defaults.headers['Authorization'] = `Bearer ${token}`;

  return token ? <Authenticated /> : <UnAuthenticated />
};

const mapStateToProps = ({ auth = {} }) => ({
  token: auth.token,
});

export default withRouter(connect(mapStateToProps)(App));
