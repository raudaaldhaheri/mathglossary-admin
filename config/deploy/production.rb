# frozen_string_literal: true

set :branch, 'master'
set :application, 'salah'
set :deploy_to, "/var/www/html/#{fetch(:application)}"

server '18.191.223.224',
       user: 'ubuntu',
       roles: %w[web app],
       ssh_options: {
         user: 'ubuntu', # overrides user setting above
         keys: %w[~/.ssh/id_rsa],
         forward_agent: false,
         auth_methods: %w[publickey]
         #  password: ''
       }
