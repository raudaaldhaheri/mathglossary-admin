# frozen_string_literal: true

set :application, 'laan-admin'
set :deploy_to, "/var/www/html/#{fetch(:application)}"
set :branch, 'dev'

server '52.220.140.207',
       user: 'ubuntu',
       roles: %w[web],
       ssh_options: {
         user: 'ubuntu', # overrides user setting above
         keys: %w[~/.ssh/id_rsa],
         forward_agent: false,
         auth_methods: %w[publickey]
         #  password: ''
       }
